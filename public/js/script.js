"use strict";

jQuery(document).ready(function() {
	var app = new App();
	app.init();
});


var App = function() {

	var debug = true,
		is_ie8 = jQuery('body.ie8').length,
		current_page,
		masterViewModel = {},
		sidebarViewModel = {},
		checkRateFormViewModel = {},
		loanOptionsViewModel = {},
		loanAppFormViewModel = {},
		default_amount = '$5,000.00',	// CTA form default amount (placeholder)
		default_amount_raw = 5000,		// CTA form default amount
		max_amount = 35000,
		min_amount = 3001,
		breadcrumb_steps = [
			{ step: 1, label: 'Check Your Rate' },
			{ step: 2, label: 'Choose Your Rate' },
			{ step: 3, label: 'Loan Terms' },
			{ step: 4, label: 'Bank Account' },
			{ step: 5, label: 'Bank Info' }
		];



	/* App init */
	this.init = function() {

		/* Init foundation */
		$(document).foundation({
			abide: {
				focus_on_invalid: false, // Causes problems on mobile
				timeout: 0, // The default is 1s, wtf?
				validators: {
					passwordStrength: function (el, required, parent) {
						return (el.value.length > 7 && el.value.match(/\d/) !== null);
					},
					loan_amount_range_check: function (el, required, parent) {
						return (el.value >= min_amount && el.value <= max_amount);
					},
					bypass_pattern: function (el, required, parent) {
						/* We need pattern='[0-9]*' on this field for mobile, which
						   triggers abide validation for this field. This serves to
						   override that behavior. */

						return true;
					}
				}
			}
		});

		this.loadListeners();
		this.loadDataBindings();
		this.loadRouter();
	};



	this.loadRouter = function() {
		var app = this;

		/* Build master view model, define global functions */
		masterViewModel = {

			/* Component view models */
		    sidebar_vm: sidebarViewModel,
			checkrate_vm: checkRateFormViewModel, 
			loanapp_vm: loanAppFormViewModel,
			loanoptions_vm: loanOptionsViewModel,

			/* 'Global' variables */
			current_step: ko.observable(0),
			loan_minimum: formatCurrency(min_amount),
			loan_maximum: formatCurrency(max_amount),
			loan_amount_requested: ko.observable(''),
			loan_amount_requested_raw: ko.observable(null),
			loan_purpose: ko.observable(''),

			/* Events */
			loadPage: function(page, callback) {
				var page = jQuery(page.element),
					form,
					app_step = page.attr('data-app-step') || 0;
				
				/* Show breadcrumbs for steps 1 and on, hide for step 0 */
				app.toggleSidebar(app_step);

				/* Scroll page to top */
				$('html, body').animate({ scrollTop: 0 }, 500);

				/* Fade in next page */
				page.fadeIn(500, function() {
					form = page.find('form');

					/* Foundation validation -- NOT supported by IE8 */
					if (!is_ie8) {
						form.foundation('abide');

						form.on('invalid.fndtn.abide', function() {
							if (debug && !is_ie8) console.log('Abide validation failed');
							form.addClass('invalid');
						}).on('valid.fndtn.abide', function() {
							form.removeClass('invalid');

							if (app.validateForm(app.current_page)) {
								app.getFormResponse();
								app.loadNextForm();
							}
						});

						return;
					}

					/* Perform manual validation without Abide */
					form.on('submit', function(e) {
						e.preventDefault();

						if (app.validateForm(app.current_page)) {
							app.getFormResponse();
							app.loadNextForm();
						}

						return;
					});
				});

				app.current_page = page;
			},

			highlight: function(data, event) {
				event.target.select();
				if (!is_ie8) event.target.setSelectionRange(0, 9999); // if the above doesn't fire (iOS)
			},

			selectLoanOption: function(data, event) {
				var loan_id = data.loan_id,
					option = $(event.target).parent('tr');

				$('[data-rate-id].selected').removeClass('selected');
				option.addClass('selected');

				loanOptionsViewModel.selected_option(loan_id);
			}
		};

		pager.Href.hash = '#!/';
		pager.extendWithPage(masterViewModel);
		ko.applyBindings(masterViewModel);

		pager.start();
	};






	this.getFormResponse = function() {
		var post_data,
			post_data_string,
			str = '',
			app = this;


		post_data = app.current_page.find('form').serializeArray();
		post_data_string = $.param(post_data);

		// console.log(post_data);

		for (var i = 0; i < post_data.length; i++) {
			switch (post_data[i].name) {
				case 'howmuch':
					masterViewModel.loan_amount_requested(post_data[i].value);
				break;
				case 'purpose':
					masterViewModel.loan_purpose(post_data[i].value);
				break;
			}
		}

		str  = 'REQUEST:<br /> ' + post_data_string + '<br /><br />RESPONSE:<br />...';
		if (debug) $('.debug-console').removeClass('hide').html(str);

		$.post('request.php', post_data_string, function(data) {
			str  = 'REQUEST:<br /> ' + post_data_string + '<br /><br />RESPONSE:<br />' + data;

			if (data) {
				data = JSON.parse(data);

				if (debug) $('.debug-console').removeClass('hide').html(str);

				if (data.loan_options) {
					loanOptionsViewModel.loanOptions(data.loan_options);
					loanOptionsViewModel.show_loan_options(true);

					var elem = $('#app [data-rate-table] tbody tr').eq(0);
					if (debug && !is_ie8) console.log(elem);
					elem.addClass('selected');
				}
			}
		});
	};




	this.loadNextForm = function() {
		var alias;

		if (this.current_page.attr('data-alias') == 'start') {
			alias = 'check-your-rate';
		} else if (this.current_page) {
			alias = this.current_page.next('.single-page').attr('data-alias');
		}

		window.top.location = '#!/' + alias;
	};






	this.loadListeners = function() {
		var app = this;


		$('#app').on('mouseover', '.button-select', function() {
			$(this).next('.button').addClass('hover');

		}).on('mouseout', '.button-select', function() {
			$(this).next('.button').removeClass('hover');

		}).on('change', '.button-select', function() {
			var selected = $(this).val(),
				selected_label = $(this).find('option:selected').text(),
				button_group = $(this).parents('.button-group');

			if (!selected || selected == '') {
				$(this).next('.button').removeClass('selected').text(function() {
					return $(this).attr('data-label');
				});
			} else {
				button_group.first().find('li .selected').removeClass('selected');
	
				button_group.find('input[type="radio"]').prop('checked', false);
				$(this).next('.button').addClass('selected').text(selected_label);
				$(this).prev('input[type="radio"]').val(selected).prop('checked', true);
			}

		}).on('click', '.button-group .button', function() {
			var button_group;

			if ($(this).is('[data-button-select]')) return;

			button_group = $(this).parents('.button-group').first();

			if ($(this).prev('input[type="radio"]:checked').length === 0) {
				button_group.find('li .selected').removeClass('selected');
				button_group.find('[data-button-select]').text(function() {
					return $(this).attr('data-label');
				});
				button_group.find('select').val('');
			}

			if (is_ie8) {
				$(this).addClass('selected');
				$(this).prev('input[type="radio"]').attr('checked', function() {
					return (!$(this).is(':checked'));
				});
			}

		}).on('click', '.fill_dummy_data', function() {
			var form = $(this).parents('form').first();

			form.find('input[data-dummy], select[data-dummy]').val(function() {
				return $(this).prop('checked', 'checked').attr('data-dummy');
			});

			form.find('input').first().click().blur();

		}).on('submit', 'form', function(e) {
			var form = $(this);

			if (form.find('input[name="app_step"]').length > 0) {
				e.preventDefault();
			}

			return false;
		});

	};





	this.validateForm = function(form) {
		var required_fields,
			completed = 0,
			app = this;

		if (typeof('form') !== undefined) {
			required_fields = form.find('[required]');
		}

		required_fields.each(function() {
			var radio_input_group = $(this).find('input[type="radio"]');

			if ($(this).is('[type="text"], [type="number"], [type="alpha"], [type="password"], [type="email"]') && $(this).val() !== '') {
				completed++;
			}
			else if ($(this).is('[type="checkbox"]') && $(this)[0].checked !== false) {
				completed++;
			}
			else if ($(this).is('select') && $(this)[0].selectedIndex !== 0) {
				completed++;
			}
			else if (radio_input_group.length) {
				radio_input_group.each(function() {
					if ($(this)[0].checked !== false) {
						completed++;
						return false;
					}
				});
			}
		});

		if (completed >= required_fields.length) {

			return true;
		} else {

			if (debug && !is_ie8) console.log (completed + ' out of ' + required_fields.length + ' completed');
			return false;
		}


	};





	this.loadDataBindings = function() {
		
		/* Sidebar */
		for (var i = 0; i < breadcrumb_steps.length; i++) {
			breadcrumb_steps[i].classes = function() {
				var classes = [];

				if (this.step == masterViewModel.current_step()) {
					classes.push('in-progress');
				} else if (this.step < masterViewModel.current_step()) {
					classes.push('complete');
				}

				return classes.join(' ');
			}
		}

		sidebarViewModel = {
			breadcrumbs: ko.observableArray(breadcrumb_steps),
			show_mobile_nav: ko.observable(false),

			toggleMobileNav: function() {
				var toggle = (sidebarViewModel.show_mobile_nav());
				if (!toggle) sidebarViewModel.show_mobile_nav(true);
				else sidebarViewModel.show_mobile_nav(null);
			}
		};







		/* Step 0 / CTA form */
		checkRateFormViewModel = {
			how_much: ko.observable(default_amount),
			how_much_raw_value: ko.observable(5000),
			display_range_error: ko.observable(false),

			money_focused: function() {
				var amt = this.how_much();
				if (amt.indexOf('.') !== -1) {
					amt = amt.substr(0, this.how_much().indexOf('.'));
				}
				amt = amt.replace(/\D/g,'');

				if (amt < min_amount || amt > max_amount) {
					this.display_range_error(true);
				} else {
					this.display_range_error(false);
				}

				if(!isNaN(amt) || amt <= 0) this.how_much(amt);
				else this.how_much('');
			},

			money_blurred: function() {
				var amt = this.how_much();
				if (amt.indexOf('.') !== -1) {
					amt = amt.substr(0, this.how_much().indexOf('.'));
				}
				amt = amt.replace(/\D/g,'');

				if (amt < min_amount || amt > max_amount) {
					this.display_range_error(true);
				} else {
					this.display_range_error(false);
				}

				if(!isNaN(amt) || amt <= 0) {
					this.how_much(formatCurrency(amt));
					this.how_much_raw_value(amt);
				}
				else {
					this.how_much(default_amount);
				}
			}
		};







		/* Step 1 / Loan application form */
		loanAppFormViewModel = {
			income_value: ko.observable(''),
			income_raw_value: ko.observable(''),
			ssn_value: ko.observable(''),
			ssn_raw_value: ko.observable(''),

			money_focused: function() {
				var amt = this.income_value();
				if (amt.indexOf('.') !== -1) {
					amt = amt.substr(0, this.income_value().indexOf('.'));
				}
				amt = amt.replace(/\D/g,'');

				if(!isNaN(amt) || amt <= 0) this.income_value(amt);
				else this.income_value('');
			},

			money_blurred: function() {
				var amt = this.income_value();
				if (amt.indexOf('.') !== -1) {
					amt = amt.substr(0, this.income_value().indexOf('.'));
				}
				amt = amt.replace(/\D/g,'');

				if(!isNaN(amt) || amt <= 0) {
					this.income_value(formatCurrency(amt));
					this.income_raw_value(amt);
				}
				else {
					this.income_value(default_amount);
				}
			},

			ssn_focused: function() {
				var amt = this.ssn_value();
				amt = amt.replace(/\D/g,'');

				if(!isNaN(amt) || amt <= 0) this.ssn_value(amt);
			},

			ssn_blurred: function() {
				var amt = this.ssn_value(),
					masked_amt = '';

				amt = amt.replace(/\D/g,'');

				if (amt.length >= 9) {
					masked_amt = amt.substr(0, 3) + '-' + amt.substr(3, 2) + '-' + amt.substr(5, 4);
				}

				if(!isNaN(amt)) {
					this.ssn_raw_value(amt);
					this.ssn_value(masked_amt);
				}
				else {
					this.ssn_value('');
				}
			}
		};







		/* Step 2 / loan options selection */
		loanOptionsViewModel = {
			loanOptions: ko.observableArray(false),
			show_loan_options: ko.observable(false),
			selected_option: ko.observable(1),

			selected_rate_text: function() {
				var loan_options = this.loanOptions().slice(),
					loan_id = this.selected_option(),
					vm = this,
					result = {};

				$.map(loan_options, function(option, i) {
					$.map(option.offers, function(rate, j) {
						if (rate.loan_id == loan_id) {
							result.term = loan_options[i].term.toString() + ' months';
							result.rate = loan_options[i].rate.toString() + '%';
							result.apr = loan_options[i].apr.toString() + '%';
							result.loan_amount = formatCurrency(loan_options[i].offers[j].loan_amount);
							result.monthly_payment = formatCurrency(loan_options[i].offers[j].monthly_payment);
						}
					});
				});

				return result;
			}
		};





		
	};



	/* Hide or show sidebar -- no parameters = hide */
	this.toggleSidebar = function(step) {
		var sidebar = $('.sidebar'),
			header = $('header'),
			items = $('.breadcrumbs-nav li'),
			track = $('.breadcrumbs-nav-track');

		if (typeof(step) !== 'undefined' && step !== 0) {
			header.addClass('show_nav');
			sidebar.removeClass('hide');

			setTimeout(function() {
				track.addClass('show');
			}, 100);

			items.each(function(i) {
				var item = $(this);

				setTimeout(function() {
					item.addClass('show');
				}, i*225 + 550);
			});

			masterViewModel.current_step(step);
		} else {

			header.removeClass('show_nav');
			sidebar.addClass('hide');
			items.add(track).removeClass('show');
		}
	};






	/* If this step was completed, show next item */
	this.nextBreadcrumb = function() {
		masterViewModel.current_step(masterViewModel.current_step() + 1);
	};



};




function formatLargeNumber(x) {
    return Math.abs(x).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}



function formatCurrency(val) {
	return '$' + formatLargeNumber(val);
}



ko.bindingHandlers.textMoney = {
	init: function(element, valueAccessor, allBindings) {
		var value = valueAccessor();
		var raw_value = parseFloat(ko.unwrap(value));

		$(element).text(formatCurrency(raw_value));
	}
};



ko.bindingHandlers.textPercentage = {
	init: function(element, valueAccessor, allBindings) {
		var value = valueAccessor();
		var raw_value = ko.unwrap(value);
		
		$(element).text(raw_value + '%');
	}
};
















