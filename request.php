<?php

sleep(1);

switch($_POST['app_step']) {

	case 0:
		echo json_encode(array('return_data' => 'Data for lead form received'));
	break;

	case 1:
		$loan_options = array();

		$loan_options[] = array(
			'term' => 36,
		 	'rate' => 9.17,
		  	'apr' => 11.26,
		  	'offers' => array(
		  		array('loan_id' => 1, 'loan_amount' => 5555, 'monthly_payment' => 177.09),
		  		array('loan_id' => 2, 'loan_amount' => 6110, 'monthly_payment' => 194.78),
		  		array('loan_id' => 3, 'loan_amount' => 6666, 'monthly_payment' => 212.50)
		  	)
		);

		$loan_options[] = array(
			'term' => 60,
		 	'rate' => 10.46,
		  	'apr' => 14.04,
		  	'offers' => array(
		  		array('loan_id' => 4, 'loan_amount' => 5555, 'monthly_payment' => 127.33),
		  		array('loan_id' => 5, 'loan_amount' => 6110, 'monthly_payment' => 140.05),
		  		array('loan_id' => 6, 'loan_amount' => 6666, 'monthly_payment' => 152.80)
		  	)
		);

		echo json_encode(array('loan_options' => $loan_options));
	break;

	case 2:
		echo json_encode(array('return_data' => 'Data for rate selection received'));
	break;
}