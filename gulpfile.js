var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

// gulp.task('build-foundation', function() {
// 	gulp.src([
// 			'sass/vendor/foundation/scss/normalize.scss',
// 			'sass/vendor/foundation/scss/foundation.scss'
// 		])
// 		.pipe(sass({
// 			outputStyle: 'compressed',
// 			includePaths: ['sass'],
// 			data: '@import "settings.scss"'
// 		}).on('error', sass.logError))
// 		.pipe(concat('foundation-5.5.3.min.css'))
// 		.pipe(gulp.dest('./css/'))
// });

gulp.task('build-css', function() {
    gulp.src('sass/main.scss')
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('./public/css/'))
});

gulp.task('default', function() {
    gulp.watch('sass/*.scss', ['build-css']);
});

