<!DOCTYPE html>
<html lang='en'>
<head>
	<title>CircleBack Lending</title>

	<meta charset='utf-8'>
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />
	<meta name='apple-mobile-web-app-capable' content='yes'>
	<meta http-equiv='X-UA-Compatible' content='IE=edge'>

	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:600,400' />

	<!--[if (gt IE 8)|!(IE)]><!-->
	<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
	<!--<![endif]-->

	<!--[if lt IE 9]>
	<link rel='stylesheet' href='css/foundation-3.2.5.min.css' />
	<script src='//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js'></script>
	<script src='//cdnjs.cloudflare.com/ajax/libs/css3pie/1.0beta5/PIE.js'></script>
	<![endif]-->

	<link rel='stylesheet' href='public/css/style.min.css' />

	<script src='//cdnjs.cloudflare.com/ajax/libs/knockout/3.4.0/knockout-min.js'></script>
	<script src='//cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/vendor/modernizr.js'></script>
	<script src='//cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation.min.js'></script>
	<script src='public/js/jquery.hashchange.min.js'></script>
	<script src='public/js/pager.min.js'></script>

	<!--[if (gt IE 8)|!(IE)]><!-->
	<script src='//cdnjs.cloudflare.com/ajax/libs/foundation/5.5.3/js/foundation/foundation.abide.min.js'></script>
	<!--<![endif]-->

	<!--[if lt IE 9]>
	<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
	<script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
	<script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
	<![endif]-->

	<script src='public/js/script.js'></script>
</head>

<!--[if (gt IE 9)|!(IE)]><!-->
<body>
<!--<![endif]-->

<!--[if lt IE 9]>
<body class="ie8">
<![endif]-->

<div class="wrapper" id="app">

	<?php include('app/header.php'); ?>

	<div class="row">
		<div class="columns medium-9 nine main-body">

			<?php
			$application_steps = [
				['step' => 1, 'source' => 'app/app1.php', 'slug' => 'check-your-rate'],
				['step' => 2, 'source' => 'app/app2.php', 'slug' => 'choose-your-rate'],
				['step' => 3, 'source' => 'app/app3.php', 'slug' => 'loan-terms'],
				['step' => 4, 'source' => 'app/app4.php', 'slug' => 'bank-account'],
				['step' => 5, 'source' => 'app/app5.php', 'slug' => 'bank-info'],
				['step' => 6, 'source' => 'app/thankyou.php', 'slug' => 'thank-you']
			];
			?>

			<?php foreach($application_steps as $step): ?>
			<div class="single-page"
				 data-alias="<?php echo $step['slug']; ?>"
				 data-app-step="<?php echo $step['step']; ?>"
				 data-bind="page: {
				 			id: '<?php echo $step['slug']; ?>',
				 			sourceOnShow: '<?php echo $step['source']; ?>',
				 			showElement: loadPage }">
			</div>
			<?php endforeach; ?>

			<div class="clearfix"></div>

		</div><!-- .main-body -->

		<?php include('app/sidebar.php'); ?>

	</div><!-- .row -->

	
	<div data-alias="start"
		 data-bind="page: { id: 'start',
		 			sourceOnShow: 'app/cta.php',
		 			showElement: loadPage }">
	</div>
</div><!-- .wrapper -->

<div class="clearfix buffer"></div>

<?php include('app/footer.php'); ?>

<div class="clearfix buffer huge"></div>
<code class="debug-console hide"></code>

</body>
</html>




