<div class="columns medium-3 three sidebar hide" data-bind="with: sidebar_vm">
	
	<div class="breadcrumbs-nav-wrapper">
		<div class="breadcrumbs-nav-track"></div>
		<ul class="breadcrumbs-nav pull-right" data-bind="foreach: breadcrumbs">
			<li data-bind="attr: { 'data-step': step }, css: classes()">
				<div class="circle"></div>
				<span data-bind="text: label"></span>
			</li>
		</ul>
	</div>

</div><!-- .sidebar -->