<!-- App 2 -->

<form data-abide="ajax" data-bind="with: loanoptions_vm">

	<h3>Choose Your Rate</h3>

	<input type="hidden" name="app_step" value="2" />

	<br />

	<div data-bind="ifnot: show_loan_options" class="text-center">

		Loading your rates...

		<br />
		<br />

		<div class="clearfix buffer"></div>
	</div>

	<div data-bind="if: show_loan_options">
		<div class="panel callout text-center">

			<h4>Congratulations! We have a great rate for you:</h4>

			<span data-bind="text: selected_rate_text().loan_amount"></span><br />
			Rate: <span data-bind="text: selected_rate_text().rate"></span>, 
			Term: <span data-bind="text: selected_rate_text().term"></span>, 
			APR: <span data-bind="text: selected_rate_text().apr"></span>, 
			Monthly payment: <span data-bind="text: selected_rate_text().monthly_payment"></span>

		</div>

		<br />

		<p class="text-center" data-bind="if: show_loan_options">You also qualify for these loan options:</p>

		<div class="row" data-bind="foreach: loanOptions">
			<div class="columns large-6 six">

				<h4 class="text-center"><span data-bind="text: term"></span> Month Loan</h4>

				<table width="100%" data-rate-table>
					<thead>
						<tr>
							<th>Amount</th>
							<th>Rate</th>
							<th>APR</th>
							<th>Payments</th>
						</tr>
					</thead>

					<tbody data-bind="foreach: offers">
						<tr data-bind="attr: { 'data-rate-id': loan_id }, event: { click: $root.selectLoanOption }">
							<td data-bind="textMoney: loan_amount"></td>
							<td data-bind="textPercentage: $parent.rate"></td>
							<td data-bind="textPercentage: $parent.apr"></td>
							<td data-bind="textMoney: monthly_payment"></td>
						</tr>
					</tbody>
				</table>

			</div>
		</div><!-- .row -->

		<div class="hidden-input">
			<input type="text" name="loan_term_option_id" data-bind="textInput: selected_option" required />
		</div>

		<div class="clearfix buffer"></div>

		<input type="submit" value="Continue" class="button right radius" />
	</div>

	<div class="clearfix"></div>
</form>






