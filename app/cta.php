<!-- CTA -->
<?php include ('loan-types.php'); ?>

<div class="row hero">
	<div class="columns small-12 twelve">
		<div class="row">
			<div class="columns large-5 medium-7 seven end">
				<div class="cta-form">

					<form data-abide="ajax" data-bind="with: checkrate_vm">
						<h4 class="text-center">Personal loans from <b>$3,001</b> to <b>$35,000</b></h4>

						<br class="show-for-medium-up" />

						<input type="hidden" name="app_step" value="0" />

						<div class="row">
							<div class="columns medium-5 five">
								<label class="inline bigger">I need to borrow...</label>
							</div>
							<div class="columns medium-7 seven">
								<input type="text"
								id="how_much_placeholder"
								pattern="[0-9]*"
								data-abide-validator="bypass_pattern"
								data-bind="textInput: how_much, event: { focus: money_focused, blur: money_blurred, click: $parent.highlight }" />

								<div class="hidden-input">
									<input type="text"
									tabindex="-1"
									data-bind="textInput: how_much_raw_value"
									required
									name="howmuch"
									data-abide-validator="loan_amount_range_check" />
								</div>
							</div>
						</div>

						<div class="row">
							<div class="columns large-12 twelve">
								<label class="bigger">What is it for?</label>
							</div>
							<div class="columns large-12 twelve">
								<ul class="button-group button-group-small toggle even-3 what-is-it-for" required data-toggle="buttons-radio">
									<li>
										<input type="radio"
										tabindex="-1"
										id="opt1"
										name="purpose"
										data-toggle="button"
										value="<?php echo $loan_types[0]['slug']; ?>">
										<label class="button secondary" for="opt1"><?php echo $loan_types[0]['label']; ?></label>
									</li>
									<li>
										<input type="radio"
										tabindex="-1"
										id="opt2"
										name="purpose"
										data-toggle="button"
										value="<?php echo $loan_types[1]['slug']; ?>">
										<label class="button secondary" for="opt2"><?php echo $loan_types[1]['label']; ?></label>
									</li>
									<li>
										<input type="radio" tabindex="-1" id="opt3" name="purpose" value="" class="hide">
										<select class="button-select" tabindex="-1">
											<option value=""></option>

											<?php foreach($loan_types['more'] as $type): ?>
											<option value="<?php echo $type['slug']; ?>"><?php echo $type['label']; ?></option>
										<?php endforeach; ?>

									</select>
									<label class="button secondary with-arrow" data-label="More Options..." data-button-select>More Options...</label>
								</li>
							</ul>
						</div>
					</div>

					<div class="row clearfix">
						<div class="columns large-12 twelve">
							<label class="bigger">How is your credit?</label>
						</div>
						<div class="columns large-12 twelve">
							<ul class="button-group even-5 toggle" required data-toggle="buttons-radio">
								<li>
									<input type="radio" tabindex="-1" id="r1" value="poor" name="credit" data-toggle="button">
									<label class="button secondary" for="r1">Poor<small>&lt; 600</small></label>
								</li>
								<li>
									<input type="radio" tabindex="-1" id="r2" value="fair" name="credit" data-toggle="button">
									<label class="button secondary" for="r2">Fair<small>600 - 660</small></label>
								</li>
								<li>
									<input type="radio" tabindex="-1" id="r3" value="good" name="credit" data-toggle="button">
									<label class="button secondary" for="r3">Good<small>660 - 720</small></label>
								</li>
								<li>
									<input type="radio" tabindex="-1" id="r4" value="excellent" name="credit" data-toggle="button">
									<label class="button secondary" for="r4">Excellent<small>720+</small></label>
								</li>
								<li>
									<input type="radio" tabindex="-1" id="r5" value="not_sure" name="credit" data-toggle="button">
									<label class="button secondary" for="r5">
										Not Sure<small><i class="question-mark"></i></small>
									</label>
								</li>
							</ul>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="text-center">
						<input type="submit" value="Continue" class="button" />
					</div>
				</form>


				</div><!-- .cta-form -->
			</div>
		</div><!-- .row -->
	</div>

	<div class="clearfix buffer"></div>

</div><!-- .row.hero -->


<?php include('testimonials.php'); ?>










