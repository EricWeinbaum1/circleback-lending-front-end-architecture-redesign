<header>
	<div class="row">

		<div class="columns small-4 four">
			<a href="#" class="logo"></a>
		</div>

		<div class="columns small-8 eight text-right">

			<div class="show-for-small-only">
				<a href="#" class="hamburger" data-bind="event: { click: sidebar_vm.toggleMobileNav }"></a>
			</div>

			<nav role="navigation" data-bind="css: { 'mobile': sidebar_vm.show_mobile_nav }">
				<ul>
					<li><a href="#">Borrow</a>

						<ul>
							<li><a href="#">How to Borrow</a></li>
							<li><a href="#">Who Can Borrow?</a></li>
							<li><a href="#">Loan Rates</a></li>
							<li><a href="#">Borrower Fees</a></li>
						</ul>

					</li>
					<li><a href="#">Buy Loans</a></li>
					<li><a href="#">About Us</a>

						<ul>
							<li><a href="#">Overview</a></li>
							<li><a href="#">Management Team</a></li>
							<li><a href="#">Advisors</a></li>
							<li><a href="#">Press</a></li>
							<li><a href="#">Contact</a></li>
							<li><a href="#">Jobs</a></li>
						</ul>

					</li>
					<li><a href="#">Loan Types</a>

						<ul>
							<li><a href="#">Home Buying</a></li>
							<li><a href="#">Car Financing</a></li>
							<li><a href="#">Major Purchase</a></li>
						</ul>

					</li>
					<li><a href="#">Sign In</a></li>
				</ul>
			</nav>

		</div>
	</div><!-- .row -->
</header>




