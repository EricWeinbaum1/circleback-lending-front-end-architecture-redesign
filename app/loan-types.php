<?php
$loan_types = [
	['slug'  => 'debt-consolidation', 'label' => 'Debt Consolidation'],
	['slug'  => 'home-improvement', 'label' => 'Home Improvement'],
	'more' => [
		['slug'  => 'cc-refinance', 'label' => 'Credit Card Refinancing'],
		['slug'  => 'major-purchase', 'label' => 'Major Purchase'],
		['slug'  => 'car-financing', 'label' => 'Car Financing'],
		['slug'  => 'business', 'label' => 'Business'],
		['slug'  => 'medical-expenses', 'label' => 'Medical Expenses'],
		['slug'  => 'vacation', 'label' => 'Vacation'],
		['slug'  => 'other', 'label' => 'Other']
	]
];