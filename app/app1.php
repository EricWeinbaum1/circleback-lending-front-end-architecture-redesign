<!-- App 1 -->
<?php include ('loan-types.php'); ?>

<form data-abide="ajax" data-bind="with: loanapp_vm">	
	<h3>Check Your Rate <a style="font-size:8.5pt" class="fill_dummy_data">(Fill form)</a></h3>

	<input type="hidden" name="app_step" value="1" />
	<input type="hidden" name="howmuch" data-bind="textInput: $parent.loan_amount_requested" />
	<input type="hidden" name="purpose" data-bind="textInput: $parent.loan_purpose" />


	<!-- Name & address -->
	<div class="row">
		<div class="columns medium-4 small-5 four">
			<label>First name
				<input type="text" id="fname" name="fname" required data-dummy="Justin" placeholder="First name" />
			</label>
			<small class="error">Please enter a valid first name.</small>
		</div>
		<div class="columns medium-6 small-7 six end">
			<label>Last name
				<input type="text" id="lname" name="lname" required data-dummy="Holt" placeholder="Last name" />
			</label>
			<small class="error">Please enter a valid last name.</small>
		</div>
	</div>
	<div class="row">
		<div class="columns medium-8 eight end">
			<label>Street address
				<input type="text" id="street" name="street" data-dummy="716 Vine St." required placeholder="Street address" />
			</label>
			<small class="error">Please enter a valid street address.</small>
		</div>
	</div><!-- .row -->

	<div class="row">
		<div class="columns medium-5 five">
			<label>City
				<input type="text" id="city" name="city" required data-dummy="Hinsdale" placeholder="City" />
			</label>
			<small class="error">Please enter a valid city.</small>
		</div>
		<div class="columns medium-3 small-6 two">
			<div class="row collapse">
				<label>State
					<select id="state" name="state" data-dummy="AL" required>
						<option value="">State</option>
						<option value="AL">Alabama</option>
						<option value="AK">Alaska</option>
						<option value="AZ">Arizona</option>
						<option value="AR">Arkansas</option>
						<option value="CA">California</option>
						<option value="CO">Colorado</option>
						<option value="CT">Connecticut</option>
						<option value="DE">Delaware</option>
						<option value="FL">Florida</option>
						<option value="GA">Georgia</option>
						<option value="HI">Hawaii</option>
						<option value="ID">Idaho</option>
						<option value="IL">Illinois</option>
						<option value="IN">Indiana</option>
						<option value="IA">Iowa</option>
						<option value="KS">Kansas</option>
						<option value="KY">Kentucky</option>
						<option value="LA">Louisiana</option>
						<option value="ME">Maine</option>
						<option value="MD">Maryland</option>
						<option value="MA">Massachusetts</option>
						<option value="MI">Michigan</option>
						<option value="MN">Minnesota</option>
						<option value="MS">Mississippi</option>
						<option value="MO">Missouri</option>
						<option value="MT">Montana</option>
						<option value="NE">Nebraska</option>
						<option value="NV">Nevada</option>
						<option value="NH">New Hampshire</option>
						<option value="NJ">New Jersey</option>
						<option value="NM">New Mexico</option>
						<option value="NY">New York</option>
						<option value="NC">North Carolina</option>
						<option value="ND">North Dakota</option>
						<option value="OH">Ohio</option>
						<option value="OK">Oklahoma</option>
						<option value="OR">Oregon</option>
						<option value="PA">Pennsylvania</option>
						<option value="RI">Rhode Island</option>
						<option value="SC">South Carolina</option>
						<option value="SD">South Dakota</option>
						<option value="TN">Tennessee</option>
						<option value="TX">Texas</option>
						<option value="UT">Utah</option>
						<option value="VT">Vermont</option>
						<option value="VA">Virginia</option>
						<option value="WA">Washington</option>
						<option value="WV">West Virginia</option>
						<option value="WI">Wisconsin</option>
						<option value="WY">Wyoming</option>
						<option value="DC">District of Columbia</option>
						<option value="AS">American Samoa</option>
						<option value="GU">Guam</option>
						<option value="MP">Northern Mariana Islands</option>
						<option value="PR">Puerto Rico</option>
						<option value="UM">United States Minor Outlying Islands</option>
						<option value="VI">Virgin Islands, U.S.</option>
					</select>
				</label>
				<small class="error">Please select a state.</small>
			</div>
		</div>
		<div class="columns medium-4 small-6 three end">
			<label>Zip
				<input type="text"
					   pattern="[0-9]*"
					   data-abide-validator="bypass_pattern"
					   name="zip"
					   id="zip"
					   required
					   data-dummy="60521"
					   placeholder="Zip" />
			</label>
			<small class="error">Please enter a valid zip code.</small>
		</div>
	</div><!-- .row -->

	<hr class="clearfix buffer" />






	<!-- DOB/SSN/Income -->

	<div class="row">
		<div class="columns medium-12 twelve">
			<div class="row collapse">
				<label>Date of birth</label>
			</div>
		</div>

		<div class="columns medium-4 small-6 four">
			<div class="row collapse">
				<label>
					<select id="dobMonth" name="dobMonth" data-dummy="March" required>
						<option value="">Month</option>		
						<option value="January">January</option>
						<option value="February">February</option>
						<option value="March">March</option>
						<option value="April">April</option>
						<option value="May">May</option>
						<option value="June">June</option>
						<option value="July">July</option>
						<option value="August">August</option>
						<option value="September">September</option>
						<option value="October">October</option>
						<option value="November">November</option>
						<option value="December">December</option>
					</select>
				</label>
				<small class="error">Select a month.</small>
			</div>
		</div>

		<div class="columns medium-2 small-3 two">
			<div class="row collapse">
				<label>
					<select id="dobDay" name="dobDay" data-dummy="16" required>
						<option value="">Day</option>
						<?php foreach(range(1,31) as $i) { ?>
						<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>
					</select>
				</label>
				<small class="error">Select a day.</small>
			</div>
		</div>

		<div class="columns medium-3 small-3 three end">
			<div class="row collapse">
				<label>
					<select id="dobYear" name="dobYear" data-dummy="1960" required>
						<option value="">Year</option>
						<?php foreach(range(1997,1900) as $i) { ?>
						<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
						<?php } ?>
					</select>
				</label>
				<small class="error">Select a year.</small>
			</div>
		</div>
	</div><!-- .row -->

	<div class="row">
		<div class="columns medium-4 small-6 six">
			<label>SSN

				<input type="text"
					   data-dummy="666-13-2853"
					   placeholder="SSN"
					   id="ssn"
					   pattern="[0-9]*"
					   data-abide-validator="bypass_pattern"
					   required
			   	   	   data-bind="textInput: ssn_value, event: { focus: ssn_focused, blur: ssn_blurred, click: $parent.highlight }" />

				<div class="hidden-input">
					<input type="text"
					data-bind="textInput: ssn_raw_value"
					data-dummy="666132853"
					tabindex="-1"
					required name="ssn" />
				</div>



			</label>
			<small class="error">Please enter a valid SSN.</small>
		</div>
		<div class="columns medium-4 small-6 three end">
			<label>Annual income

			<input type="text"
				   id="income"
				   data-dummy="$35,000.00"
				   pattern="[0-9]*"
				   data-abide-validator="bypass_pattern"
				   placeholder="Annual income"
				   required
			   	   data-bind="textInput: income_value, event: { focus: money_focused, blur: money_blurred, click: $parent.highlight }" />

			<div class="hidden-input">
				<input type="text"
				data-bind="textInput: income_raw_value"
				data-dummy="35000"
				tabindex="-1"
				required name="income" />
			</div>

			</label>
			<small class="error">Please enter a valid amount.</small>
		</div>
	</div><!-- .row -->

	<hr class="clearfix buffer" />






	<!-- Account/email/password -->

	<div class="row">
		<div class="columns large-4 medium-6 four">
			<label>Email address
				<input type="email"
					   name="email"
					   id="email"
					   data-dummy="test93254820117@aol.com"
					   required
					   placeholder="Email address" />
			</label>
			<small class="error">Please enter a valid email address.</small>
		</div>
		<div class="columns large-4 medium-6 four end">
			<label>Confirm email address
				<input type="email"
					   id="confirm_email"
					   data-equalto="email"
					   data-dummy="test93254820117@aol.com"
					   required
					   placeholder="Email address" />
			</label>
			<small class="error">Please confirm your email address.</small>
		</div>
		<div class="columns medium-12 twelve">
			<small class="info">We use your email address to contact you. Also, you can use your email address
					to sign in to your account.</small>
		</div>
	</div><!-- .row -->

	<div class="row">
		<div class="columns large-4 medium-6 four">
			<label>Create a password
				<input type="password" id="password" name="password" data-dummy="test1234" required data-abide-validator="passwordStrength" placeholder="Password" />
			</label>
			<small class="error">Please enter a valid password.</small>
		</div>
		<div class="columns large-4 medium-6 four end">
			<label>Confirm password
				<input type="password" id="confirm_password" data-equalto="password" data-dummy="test1234" required placeholder="Password" />
			</label>
			<small class="error">Please confirm your password.</small>
		</div>
		<div class="columns medium-12 twelve">
			<small class="info">Must be 8 characters or more and include at least 1 number.</small>
		</div>
	</div><!-- .row -->

	<hr class="clearfix buffer" />





	<!-- Legal -->

	<div class="row">
		<div class="columns small-12 twelve">
		
			<div class="row">
				<div class="columns medium-5 small-12 five">
					<div class="row collapse">
						<label>I have read and agree to the following:</label>
					</div>
				</div>

				<div class="columns medium-7 small-12 seven">
					<input id="terms_of_use" type="checkbox" name="agree_terms_of_use" data-dummy="1" value="1" required />
					<label class="inline" for="terms_of_use">Terms of Use<br />
						<a href="#" target="_blank" class="label-link">(view document)</a>
					</label>
				</div>
			</div>
			
			<div class="row clearfix">
				<div class="columns medium-5 five show-for-medium-up">&nbsp;</div>
				<div class="columns medium-7 small-12 seven">
					<input id="arb_agreement" type="checkbox" name="agree_arb_agreement" data-dummy="1" value="1" required />
					<label class="inline" for="arb_agreement">Arbitration Agreement<br />
						<a href="#" target="_blank" class="label-link">(view document)</a>
					</label>
				</div>
			</div>
			
			<div class="row clearfix">
				<div class="columns medium-5 five show-for-medium-up">&nbsp;</div>
				<div class="columns medium-7 small-12 seven">
					<input id="credit_auth" type="checkbox" name="agree_credit_auth" data-dummy="1" value="1" required />
					<label class="inline" for="credit_auth">Credit Profile Authorization<br />
						<a href="#" target="_blank" class="label-link">(view document)</a>
					</label>
				</div>
			</div>

		</div>
	</div><!-- .row -->




	<div class="clearfix buffer"></div>

	<input type="submit" value="Continue" class="button right" />

	<div class="clearfix"></div>
</form>








